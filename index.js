const express = require('express')
const bodyParser = require('body-parser')
const mongoclient = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectId
const assert = require('assert')
const expressMongoDb = require('express-mongo-db')

// mongodb setup, returns a db object
const url = 'mongodb://localhost:27017/blog'
const dbname = 'blog'

// function to get all posts from database as an array
function getPosts(db, callback) {
    db.collection('posts').find({}).toArray((err, result) => {
        assert.equal(null, err)
        callback(result)
    })
}

// function to add new post data
function addPost(db, title, body, callback) {
    db.collection('posts').insert({title: title, body: body}, () => {
        callback()
    })
}

// function to delete post by id
function deletePost(db, id, callback) {
    db.collection('posts').remove({_id: ObjectId(id)}, (err, obj) => {
        callback()
    })
}

/////////////////////////////////////////////////////////////////////////////

// return flags to jade from each method so that the template knows which errors to render

// express and pug setup
const app = express()

app.set('view engine', 'pug')
app.set('views', './views')
app.use(bodyParser.urlencoded({extended: true}))
app.use(expressMongoDb(url))

// root get
app.get('/', (req, res) => {
    getPosts(req.db, (posts) => { 
        res.status(200).render('index', { posts: posts })
    })
})

// add post
app.post('/add_post', (req, res) => {
    addPost(req.db, req.body.title, req.body.body, () => {
        res.redirect('/')
    })
})

// delete get
app.get('/delete_post', (req, res) => {
    deletePost(req.db, req.query.id, () => {
        res.redirect('/')
    })
})

// listening
app.listen(3000, () => {
    console.log('listening on 3000...')
})
